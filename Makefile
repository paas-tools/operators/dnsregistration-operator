# Based on Kubebuilder
# Removed codegen functionality and retained kustomization

# NOTE: there is no default for the image, it needs to be set manually for deploying
#IMG ?= "controller:latest"

# Install CRDs into a cluster
install:
	kustomize build deploy/crds | kubectl apply -f -

# Uninstall CRDs from a cluster
uninstall:
	kustomize build deploy/crds | kubectl delete -f -

# Run go fmt against code
fmt:
	go fmt ./...

# Run go vet against code
vet:
	go vet ./...

# Deploy controller in the configured Kubernetes cluster in ~/.kube/config
deploy: IMG-defined
	cd deploy && kustomize edit set image REPLACE_IMAGE=${IMG}
	kustomize build environments | kubectl apply -f -

# Build the docker image
docker-build: IMG-defined
	docker build . -t ${IMG}

# Push the docker image
docker-push: IMG-defined
	docker push ${IMG}

IMG-defined:
	@:$(call check_defined, IMG, Please define docker IMG)

# Check that given variables are set and all have non-empty values,
# die with an error otherwise.
#
# Params:
#   1. Variable name(s) to test.
#   2. (optional) Error message to print.
# https://stackoverflow.com/questions/10858261/abort-makefile-if-variable-not-set
check_defined = \
    $(strip $(foreach 1,$1, \
        $(call __check_defined,$1,$(strip $(value 2)))))
__check_defined = \
    $(if $(value $1),, \
      $(error Undefined $1$(if $2, ($2))))
