# dnsregistration-operator

High-level DNSRegistration resources that Applications can request DNSEndpoints with. Hides the complexity of multiple views in CERN DNS.

## Deployment

#### Image

Gitlab CI is set up to automatically tag images in this repo's registry whenever any branch is pushed.

#### Deploy k8s resources

Kubebuilder uses Makefiles for automation. To deploy manually:

```bash
$ make deploy IMG=<full image tag>
```
