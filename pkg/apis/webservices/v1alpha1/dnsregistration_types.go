package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// DNSRegistrationSpec defines the desired state of DNSRegistration
type DNSRegistrationSpec struct {
	// Target [optional] shows where all the fqdns should point to. Defaults to the cluster's ingress nodes.
	Target string `json:"target,omitempty"`
}

// DNSRegistrationStatus defines the observed state of DNSRegistration
type DNSRegistrationStatus struct {
	// ProvisioningStatus shows whether:
	// - Created: the FQDN has been successfully provisioned
	// - Creating: provisioning is still in progress
	// - ProvisioningError: provisioning has failed and won't be retried; look at ErrorMessage for details.
	//   It is set for permanent errors, such as name conflicts or not supporting the DNS zone.
	// +kubebuilder:validation:Required
	// +kubebuilder:validation:Enum:="Created";"Creating";"ProvisioningError"
	ProvisioningStatus string `json:"provisioningStatus,omitempty"`

	// ErrorMessage will be set in the event of a terminal error reconciling (or provisioning) the
	// DNSRegistration with a human-readable message.
	// This won't be set for transient errors where reconciliation will be reattempted.
	// +optional
	ErrorMessage string `json:"errorMessage,omitempty"`
	// ErrorReason will be set in the event of a terminal error reconciling (or provisioning) the
	// DNSRegistration with a machine-readable message.
	// This won't be set for transient errors where reconciliation will be reattempted.
	// +optional
	ErrorReason string `json:"errorReason,omitempty"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// DNSRegistration is the Schema for the dnsregistrations API
// +kubebuilder:subresource:status
// +kubebuilder:resource:path=dnsregistrations,scope=Namespaced
type DNSRegistration struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   DNSRegistrationSpec   `json:"spec,omitempty"`
	Status DNSRegistrationStatus `json:"status,omitempty"`
}

// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object

// DNSRegistrationList contains a list of DNSRegistration
type DNSRegistrationList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []DNSRegistration `json:"items"`
}

func init() {
	SchemeBuilder.Register(&DNSRegistration{}, &DNSRegistrationList{})
}
