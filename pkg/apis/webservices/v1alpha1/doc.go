// Package v1alpha1 contains API Schema definitions for the webservices v1alpha1 API group
// +k8s:deepcopy-gen=package,register
// +groupName=webservices.cern.ch
package v1alpha1
