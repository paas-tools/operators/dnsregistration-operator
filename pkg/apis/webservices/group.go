// Package webservices contains webservices API versions.
//
// This file ensures Go source parsers acknowledge the webservices package
// and any child packages. It can be removed if any other Go source files are
// added to this package.
package webservices
