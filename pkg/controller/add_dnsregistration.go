package controller

import (
	"gitlab.cern.ch/paas-tools/operators/dnsregistration-operator/pkg/controller/dnsregistration"
)

func init() {
	// AddToManagerFuncs is a list of functions to create controllers and add them to a manager.
	AddToManagerFuncs = append(AddToManagerFuncs, dnsregistration.Add)
}
