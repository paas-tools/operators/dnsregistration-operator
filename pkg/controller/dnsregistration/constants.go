package dnsregistration

// UnsupportedError means that the attempted operation is not supported by the current configuration
type UnsupportedError string

const (
	// DomainNotManagedError The input domain is not in the list of managed domains
	DomainNotManagedError UnsupportedError = "DomainNotManaged"

	// InvalidLabels The retrived resource doesn't has the necessary labels, meaning that it was possibly manually created
	InvalidLabels UnsupportedError = "InvalidLabelsOnDNSRegistration"

	// APIGroup group of the CRD that is going to be used to register the CRD in the manager scheme
	APIGroup = "dns-manager.webservices.cern.ch"
	// APIVersion version of the CRD that is going to be used to register the CRD in the manager scheme
	APIVersion = "v1alpha1"
	// DefaultTTL to be used when creating Endpoints
	DefaultTTL = 60
	// finalizerStr string that is going to added to every DNSEndpoint created
	finalizerStr = "cleanupEndpoints"
)
