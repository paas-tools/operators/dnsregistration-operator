package dnsregistration

import (
	webservicesv1alpha1 "gitlab.cern.ch/paas-tools/operators/dnsregistration-operator/pkg/apis/webservices/v1alpha1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime/schema"
	endpt "sigs.k8s.io/external-dns/endpoint"
)

// Instantiate and initialize the Endpoint structure
func createEndpoint(fqdn, target string) *endpt.Endpoint {
	return &endpt.Endpoint{
		DNSName:    fqdn,
		Targets:    []string{target},
		RecordTTL:  endpt.TTL(DefaultTTL),
		RecordType: "CNAME",
	}
}

// Instanciate and initialize the DNSEndpoint structure
func createDNSEndpoint(name, namespace string, endpoints []*endpt.Endpoint) *endpt.DNSEndpoint {
	dnsEndpoint := &endpt.DNSEndpoint{
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: namespace,
		},
		Spec: endpt.DNSEndpointSpec{
			Endpoints: endpoints,
		},
	}

	gvk := schema.FromAPIVersionAndKind(APIGroup+"/"+APIVersion, "DNSEndpoint")
	dnsEndpoint.SetGroupVersionKind(gvk)

	return dnsEndpoint
}

// setOwnerRefs adds ownerReference as labels, based on the suggestion from
// https://github.com/operator-framework/operator-sdk/blob/master/doc/ansible/dev/retroactively-owned-resources.md#for-objects-which-are-not-in-the-same-namespace-as-the-owner-cr
// but optimized for retrieval
func setOwnerRefs(dnsEndp *endpt.DNSEndpoint, dnsreg *webservicesv1alpha1.DNSRegistration) {
	labels := map[string]string{
		"operator-sdk/primary-resource-namespace": dnsreg.GetNamespace(),
		"operator-sdk/primary-resource-name":      dnsreg.GetName(),
		"operator-sdk/primary-resource-type":      dnsreg.GetObjectKind().GroupVersionKind().GroupKind().String(),
	}

	setLabels(dnsEndp, labels)
}

func setDomainViewLabels(dnsEndp *endpt.DNSEndpoint, domain, view string) {
	labels := map[string]string{
		"dns-manager.webservices.cern.ch/domain": domain,
		"dns-manager.webservices.cern.ch/view": view,
	}
	setLabels(dnsEndp, labels)
}

func setLabels(dnsEndp *endpt.DNSEndpoint, newLabels map[string]string) {
	labels := dnsEndp.GetLabels()
	if labels == nil {
		labels = newLabels
	} else {
		for key, value := range newLabels {
			labels[key] = value
		}
	}
	dnsEndp.SetLabels(labels)
}
