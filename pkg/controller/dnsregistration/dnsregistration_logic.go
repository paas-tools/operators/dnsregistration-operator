package dnsregistration

import (
	"context"
	"errors"
	"reflect"
	"strings"

	"github.com/go-logr/logr"
	webservicesv1alpha1 "gitlab.cern.ch/paas-tools/operators/dnsregistration-operator/pkg/apis/webservices/v1alpha1"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	endpt "sigs.k8s.io/external-dns/endpoint"
)

var (
	// DefaultTarget is populated by cmdline args and is the default value when a DNSRegistration doesn't have a target
	DefaultTarget string
	// DomainGroupsMap is populated by cmdline args and is the map {zone -> supported domains}
	// eg {"web.cern.ch" : {"internal.web.cern.ch","external.web.cern.ch"}}
	DomainGroupsMap map[string][]string
	// OperatorNamespace is the namespace where dnsregistration-operator and external-dns are running.
	// This is where the DNSEndpoints will be created.
	OperatorNamespace string
)

// cleanupDNSRegistration performs all necessary actions to remove the finalizers (and removes them)
func cleanupDNSRegistration(ctx context.Context, c client.Client, log logr.Logger, dnsreg *webservicesv1alpha1.DNSRegistration) (reconcile.Result, error) {
	// 1. Check for all associated DNSEndpoints
	ownedEndpoints, err := fetchOwnedEndpoints(ctx, c, dnsreg)
	if err != nil {
		log.Error(err, "Unable to fetch owned DNSEndpoints")
		return reconcile.Result{Requeue: true}, err
	}
	// 2. If there are no associated DNSEndpoints, remove finalizer
	if len(ownedEndpoints) == 0 {
		remainingFinalizers := dnsreg.GetFinalizers()
		for i, finalizer := range remainingFinalizers {
			if finalizer == finalizerStr {
				remainingFinalizers = append(remainingFinalizers[:i], remainingFinalizers[i+1:]...)
				break
			}
		}
		dnsreg.SetFinalizers(remainingFinalizers)
		return reconcile.Result{}, c.Update(ctx, dnsreg)
	}
	// 3. Request the deletion of all associated DNSEndpoints and REQUEUE (even if no error)
	for _, endpoint := range ownedEndpoints {
		if err := c.Delete(ctx, &endpoint); err != nil {
			log.Error(err, "Can't delete owned DNSEndpoint", "endpoint", endpoint)
		}
	}
	return reconcile.Result{Requeue: true}, err
}

// ensureFinalizer ensures that the DNSRegistration is flagged for its eventual cleanup
func ensureFinalizer(ctx context.Context, c client.Client, dnsreg *webservicesv1alpha1.DNSRegistration) error {
	if !contains(dnsreg.GetFinalizers(), finalizerStr) {
		dnsreg.SetFinalizers(append(dnsreg.GetFinalizers(), finalizerStr))
		return c.Update(ctx, dnsreg)
	}
	return nil
}

// handleFqdn takes a list of FQDNS and parses each one into its {target, site, domain}, then reconciles it with its DNSEndpoints
func handleFqdn(ctx context.Context, c client.Client, log logr.Logger, dnsreg *webservicesv1alpha1.DNSRegistration) (reconcile.Result, error) {
	defer c.Status().Update(context.TODO(), dnsreg)

	// 1. Check if domain is supported, if no check if the error was already reported in the status
	_, domain, err := parseDomain(dnsreg.Name, DomainGroupsMap)
	if err != nil && dnsreg.Status.ProvisioningStatus != "ProvisioningError" {
		// Set the status error message
		dnsreg.Status = webservicesv1alpha1.DNSRegistrationStatus{
			ProvisioningStatus: "ProvisioningError",
			ErrorMessage:       "This domain is not supported by DNSRegistration",
			ErrorReason:        err.Error(),
		}

		return reconcile.Result{}, err
	}

	// 2. If the domain is not supported and the error was already reported just return
	if err != nil {
		return reconcile.Result{}, nil
	}

	// 3. Generate endpoint with fqdn info
	endpointSet := []*endpt.Endpoint{createEndpoint(dnsreg.Name, defaultStr(dnsreg.Spec.Target, DefaultTarget))}

	// 4. For each view in the domain of the fqdn
	for _, view := range DomainGroupsMap[domain] {
		// We only have interest in the beggining part of the view string (eg. internal.webtest.cern.ch --> internal)
		view = strings.Split(view, ".")[0]
		// Lambda, called later
		_createEndpoint := func() (reconcile.Result, error) {
			dnsEndpoint := createDNSEndpoint(dnsEndpointNameConvention(dnsreg, view), OperatorNamespace, endpointSet)
			setOwnerRefs(dnsEndpoint, dnsreg)
			setDomainViewLabels(dnsEndpoint, domain, view)
			if err := c.Create(ctx, dnsEndpoint); err != nil {
				dnsreg.Status = webservicesv1alpha1.DNSRegistrationStatus{
					ProvisioningStatus: "Creating",
				}
				return reconcile.Result{Requeue: true}, err
			}
			return reconcile.Result{}, nil
		}

		// 5. Get existing owned DNSEndpoints for specific domain/view
		ownedEndpoint, err := fetchOwnedDNSEndpoint(ctx, c, dnsreg, domain, view)
		if err != nil {
			if apierrors.IsNotFound(err) {
				return _createEndpoint()
			}
			log.Error(err, "Unable to fetch owned DNSEndpoints")
			dnsreg.Status = webservicesv1alpha1.DNSRegistrationStatus{
				ProvisioningStatus: "Creating",
			}
			return reconcile.Result{Requeue: true}, err
		}

		// 6. Check if theres already a DNSEndpoint created, update it if not create a new DNSEndpoint
		if !reflect.DeepEqual(ownedEndpoint.Spec.Endpoints, endpointSet) {
			ownedEndpoint.Spec.Endpoints = endpointSet
			// Update the DNSEndpoint to contain the most recent updates
			if err := c.Update(ctx, ownedEndpoint); err != nil {
				dnsreg.Status = webservicesv1alpha1.DNSRegistrationStatus{
					ProvisioningStatus: "Creating",
				}
				return reconcile.Result{Requeue: true}, err
			}
		}
	}

	// 7. update the status of FQDNS
	// Set the status error message
	dnsreg.Status = webservicesv1alpha1.DNSRegistrationStatus{
		ProvisioningStatus: "Created",
	}

	//Update status for fqdns
	return reconcile.Result{}, nil
}

// validateDomain looks up the domain in the map of supported domains
func validateDomain(domain []string, domainList map[string][]string) bool {
	_, ok := domainList[strings.Join(domain, ".")]
	return ok
}

// defaultStr replaces an empty `val` with a `defVal`
func defaultStr(val, defVal string) string {
	if val == "" {
		return defVal
	}
	return val
}

// parseDomain finds the longest supported domain for the `fqdns`
// eg mysite.a.b.web.cern.ch -> web.cern.ch
func parseDomain(fqdns string, managedDomains map[string][]string) (site, domain string, err error) {
	splitFqdns := strings.Split(fqdns, ".")
	// offset of a supported domain in fqdns
	i := 1
	for ; i < len(splitFqdns) && !validateDomain(splitFqdns[i:], managedDomains); i++ {
	}
	if i == len(splitFqdns) {
		site = ""
		domain = ""
		err = errors.New(string(DomainNotManagedError))
		return
	}
	site = strings.Join(splitFqdns[0:i-1], ".")
	domain = strings.Join(splitFqdns[i:], ".")
	err = nil
	return
}

// dnsEndpointNameConvention naming convention for DNSEndpoints agreed on https://gitlab.cern.ch/webservices/webframeworks-planning/issues/7#note_3187892
func dnsEndpointNameConvention(dnsreg *webservicesv1alpha1.DNSRegistration, view string) string {
	return dnsreg.Name + "-" + view
}

func contains(a []string, x string) bool {
	for _, n := range a {
		if x == n {
			return true
		}
	}
	return false
}

// fetchOwnedDNSEndpoint gets DNSEndpoint owned by this DNSRegistration and ensures that it has all the required labels before returning it
func fetchOwnedDNSEndpoint(ctx context.Context, c client.Client, dnsreg *webservicesv1alpha1.DNSRegistration, domain, view string) (*endpt.DNSEndpoint, error) {
	existingDNSEnpt := &endpt.DNSEndpoint{}
	filter := types.NamespacedName{
		Name:      dnsEndpointNameConvention(dnsreg, view),
		Namespace: OperatorNamespace,
	}
	if err := c.Get(ctx, filter, existingDNSEnpt); err != nil {
		return nil, err
	}

	labels := map[string]string{
		"operator-sdk/primary-resource-namespace": dnsreg.GetNamespace(),
		"operator-sdk/primary-resource-name":      dnsreg.GetName(),
		"operator-sdk/primary-resource-type":      dnsreg.GetObjectKind().GroupVersionKind().GroupKind().String(),
		"dns-manager.webservices.cern.ch/domain":  domain,
		"dns-manager.webservices.cern.ch/view":    view,
	}

	//Check that the object has the desired labels
	for key, val := range labels {
		if existingDNSEnpt.GetLabels()[key] != val {
			return nil, errors.New(string(InvalidLabels))
		}
	}

	return existingDNSEnpt, nil
}

// fetchOwnedEndpoints lists DNSEndpoints owned by this DNSRegistration across all namespaces
func fetchOwnedEndpoints(ctx context.Context, c client.Client, dnsreg *webservicesv1alpha1.DNSRegistration) ([]endpt.DNSEndpoint, error) {
	endpointList := &endpt.DNSEndpointList{}
	if err := c.List(ctx, endpointList, client.MatchingLabels{
		"operator-sdk/primary-resource-namespace": dnsreg.GetNamespace(),
		"operator-sdk/primary-resource-name":      dnsreg.GetName(),
		"operator-sdk/primary-resource-type":      dnsreg.GetObjectKind().GroupVersionKind().GroupKind().String(),
	}); err != nil {
		return nil, err
	}
	return endpointList.Items, nil
}
