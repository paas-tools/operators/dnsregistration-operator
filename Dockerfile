FROM golang:1.13 AS builder
ENV GO111MODULE=on

# Copy the code from the host and compile it
WORKDIR $GOPATH/src/gitlab.cern.ch/paas-tools/operators/dnsregistration-operator
COPY . ./
RUN go mod vendor
RUN CGO_ENABLED=0 GOOS=linux go build \
      -o /dnsregistration-operator \
      -gcflags all=-trimpath=/root/go/src/gitlab.cern.ch/paas-tools/operators \
      -asmflags all=-trimpath=/root/go/src/gitlab.cern.ch/paas-tools/operators -mod=vendor \
      gitlab.cern.ch/paas-tools/operators/dnsregistration-operator/cmd/manager

FROM registry.access.redhat.com/ubi8/ubi-minimal:latest

ENV OPERATOR=/usr/local/bin/dnsregistration-operator \
    USER_UID=1001 \
    USER_NAME=dnsregistration-operator

# install operator binary
COPY --from=builder /dnsregistration-operator ${OPERATOR}

COPY build/bin /usr/local/bin
RUN  /usr/local/bin/user_setup

ENTRYPOINT ["/usr/local/bin/entrypoint"]

USER ${USER_UID}
